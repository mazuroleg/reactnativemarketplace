import { configureStore } from "@reduxjs/toolkit";
import productReducer from './itemSlice'
export default configureStore({
    reducer:{
        product:productReducer,
    }
})