import React from 'react';
import { Text, View } from 'react-native';
import { Provider } from 'react-redux';
import Main from './componets/Main';
import Navigate from './navigate/navigate';
import store from './store/index'

const App = () => {
  return (
   
   <Provider store={store}>
      <Navigate/>
      
   </Provider>
    
  )
}
export default App;