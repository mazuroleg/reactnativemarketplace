import React from 'react';
import Main from '../componets/Main';
import itemDescriptions from '../screens/ItemDescripions';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
 const Stack = createStackNavigator();
 export default function Navigate() {
        return <NavigationContainer>
                  <Stack.Navigator>
                      <Stack.Screen
                      name='Main'
                      component={Main}
                      options={{title:'головна'}}
                      
                      />
                      <Stack.Screen
                      name='itemDescription'
                      component={itemDescriptions}
                      options={{title:'Опис'}}
                      
                      />
                  </Stack.Navigator>


        </NavigationContainer>



 }