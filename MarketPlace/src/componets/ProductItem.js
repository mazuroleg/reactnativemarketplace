import React from 'react';
import {  Text, StyleSheet, View,Image } from 'react-native';


const productItem = ({ item }) =>  {
    
  return (
    
      <View style={styles.item}>
        <Image></Image>
        
        <Text  style={styles.title}>{item.title}</Text>
        
        <Text  style={styles.description}>{item.description}</Text>
        <Text  style={styles.price}>{item.price}</Text>
        
      </View>
    );
  
};
const styles=StyleSheet.create({
  item: {
    flexDirection: 'row',
    backgroundColor: '#fafafa',
    padding: 20,
    marginVertical: 16,
    marginHorizontal: 16,
    borderRadius:5,
    borderWidth:1,
    width: '90%',
  },
  title: {
    flex:1,
    fontSize: 32,
  },
  description:{
    fontSize: 16,
    flex:1,
  },
  price:{
    fontSize: 28,
    flex:1,
  },
});
export default productItem;