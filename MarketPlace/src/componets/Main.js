import React, { useEffect } from 'react';
import { FlatList, StatusBar, StyleSheet, View,Text,Button } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useDispatch, useSelector } from 'react-redux';
import { fetchProducts } from '../store/itemSlice';

import Item from './ProductItem';
import {gStyle} from '../style';
import { Header } from './Header';

const Main= ({navigation}) => {
    const products=useSelector(state=>state.product.products)
    const dispatch=useDispatch();
    useEffect(()=>{
        dispatch(fetchProducts());
        },[dispatch]);
       const renderItem = ({ item }) => (
        <Item item={item} />
      );
      const loadDescription=()=>{
        navigation.navigate('itemDescription')
    }
  return (
      
      <SafeAreaView style={gStyle.main}>
          
          <View>
              <Header/>
              <Button title='Показати опис' onPress={loadDescription}/>
          </View>
          <View>
          <FlatList
              data={products}
              renderItem={renderItem}
              keyExtractor={item => item.id}
              
          />

          </View>
         
      </SafeAreaView>
     
      
        );
}
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     marginTop: StatusBar.currentHeight || 0,
//   },
 
//});
export default Main;